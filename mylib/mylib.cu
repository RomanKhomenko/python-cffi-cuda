#include <cstdint>
#include <iostream>

template<class T>
class ArrayWrapper2D {
public:
    __host__ __device__
    ArrayWrapper2D(T* data, std::int64_t n, std::int64_t m)
        : Data{data},
          N{n},
          M{m} {}
   
    __host__ __device__ 
    T& operator()(std::int64_t i, std::int64_t j) {
        return Data[i * M + j];
    }

private:
    T* Data; 
    std::int64_t N;
    std::int64_t M;
};

__global__
void add_kernel(double* x_raw, double* y_raw, std::int64_t n, std::int64_t m) {
    auto x = ArrayWrapper2D<double>(x_raw, n, m);
    auto y = ArrayWrapper2D<double>(y_raw, n, m);

    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (std::int64_t i = index; i < n; i += stride) {
        for (std::int64_t j = 0; j < m; j++) {
            y(i, j) += x(i, j);
        }
    }
}

#define cudaCheckError() \
    do { \
        auto err = cudaGetLastError(); \
        if (err != cudaSuccess) { \
            std::cout << "Cuda failure " << __FILE__ \
                      << ":" << __LINE__ \
                      << ": " << cudaGetErrorString(err) \
                      << std::endl; \
            std::exit(EXIT_FAILURE); \
        } \
    } \
    while(false)

extern "C" void add(double* h_x, double* h_y, std::int64_t n, std::int64_t m) {
    const std::size_t SIZE = sizeof(double) * n * m;

    double* d_x;
    double* d_y;

    cudaMalloc(&d_x, SIZE);
    cudaCheckError();

    cudaMalloc(&d_y, SIZE);
    cudaCheckError();
    
    cudaMemcpy(d_x, h_x, SIZE, cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, h_y, SIZE, cudaMemcpyHostToDevice);
    cudaCheckError();

    add_kernel<<<4, 200>>>(d_x, d_y, n, m);

    cudaDeviceSynchronize();

    cudaMemcpy(h_y, d_y, SIZE, cudaMemcpyDeviceToHost);

    cudaFree(d_x);
    cudaFree(d_y);
}

