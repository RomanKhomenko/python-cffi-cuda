import numpy as np
import cffi

ffi = cffi.FFI()
ffi.cdef("void add(double*, double*, int64_t, int64_t);")
mylib = ffi.dlopen("./mylib/libmylib.so")

n = 15000
m = 15000

x = np.empty((n, m), dtype=np.float64)
x.fill(5)

y = np.empty((n, m), dtype=np.float64)
y.fill(6)

x_ptr = ffi.cast("double*", x.ctypes.data)
y_ptr = ffi.cast("double*", y.ctypes.data)

mylib.add(x_ptr, y_ptr, n, m)

print(y[22][600])
